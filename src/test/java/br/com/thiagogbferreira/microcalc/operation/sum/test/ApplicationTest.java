package br.com.thiagogbferreira.microcalc.operation.sum.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.rule.OutputCapture;

import br.com.thiagogbferreira.microcalc.operation.sum.SumOperationController;

public class ApplicationTest {
  @Rule
  public OutputCapture capture = new OutputCapture();

  /**
  * Validate if spring boot application is starting calling the main method
  */
  @Test
  public void springBootStartingTest() {
    System.setProperty("server.port", "0");
    SumOperationController.main(new String[] {});
    assertThat(capture.toString()).contains("Started SumOperationController in");
  }
}
