package br.com.thiagogbferreira.microcalc.operation.sum;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Thiago Ferreira
 *
 */
@SpringBootApplication
@RestController
public class SumOperationController {

  /**
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(SumOperationController.class, args);
  }
  
  /**
   * Rest endpoint to sum all parameters
   * @param params to sum
   * @return sum of all params
   */
  @RequestMapping("/{params:.+}")
  public ResponseEntity<BigDecimal> sum(@PathVariable("params") List<BigDecimal> params) {
    Optional<BigDecimal> result = params.stream().reduce((a, b) -> a.add(b));
    if (result.isPresent()) {
      return ResponseEntity.ok(result.get());
    }
    return ResponseEntity.badRequest().body(null);
  }

}
